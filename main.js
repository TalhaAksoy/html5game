document.addEventListener("DOMContentLoaded", () => {
        const canvas = document.querySelector("canvas");
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        const game = new Game(canvas);
        let lastTime = performance.now();

        function gameLoop(currentTime) {
                const deltaTime = currentTime - lastTime;
                lastTime = currentTime;

                game.update(deltaTime / (1000 / 60));
                game.draw(deltaTime / (1000 / 60));

                requestAnimationFrame(gameLoop);
        }

        game.load();
        requestAnimationFrame(gameLoop);
});
