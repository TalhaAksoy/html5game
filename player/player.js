class Player {
	constructor(speed, height, width, locationX, locationY) {
		this.speed = speed;
		this.height = height;
		this.width = width;
		this.locationX = locationX;
		this.locationY = locationY;
		this.keyPresses = {
			w: false,
			a: false,
			s: false,
			d: false
		};
		this.MouseClick = {
			Left: 1,
			Middle: 2,
			Right: 3,
		}

	}

	load() {
		document.addEventListener('mousedown', (event) => {
			switch(event.which){
				case this.MouseClick.Left:
					playerLeftClick(event.clientX , this.locationX.toFixed(0))
					return;
				case this.MouseClick.Middle:
					console.log("Middle Click Press");
					return;
				case this.MouseClick.Right:
					console.log("Right Click Press");
					return;
			}
		})
		document.addEventListener('keydown', (event) => this.handleKeyDown(event));
		document.addEventListener('keyup', (event) => this.handleKeyUp(event));
	}

	update(deltaTime) {
		if (this.keyPresses.w) {
			this.locationY -= this.speed * (deltaTime);
		} else if (this.keyPresses.s) {
			this.locationY += this.speed * (deltaTime);
		}
		if (this.keyPresses.a) {
			this.locationX -= this.speed * (deltaTime);
		} else if (this.keyPresses.d) {
			this.locationX += this.speed * (deltaTime);
		}
	}

	draw(ctx) {
		ctx.fillStyle = "black";
		ctx.fillRect(this.locationX, this.locationY, this.width, this.height);
	}

	handleKeyDown(event) {
		const key = event.key.toLowerCase();
		if (key in this.keyPresses) {
			this.keyPresses[key] = true;
		}
	}

	handleKeyUp(event) {
		const key = event.key.toLowerCase();
		if (key in this.keyPresses) {
			this.keyPresses[key] = false;
		}
	}
}