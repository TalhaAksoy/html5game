class Game {
	constructor(canvas) {
		this.canvas = canvas;
		this.ctx = this.canvas.getContext("2d");
		this.player = null;
		this.fps = 0;
	}

	load() {
		document.addEventListener('contextmenu' , e => e.preventDefault());
		this.player = new Player(5, 70, 30, 20, 20);
		if (this.player != null)
			this.player.load()
	}

	update(deltaTime) {
		this.player.update(deltaTime);
	}
	
	draw(deltaTime) {
		this.ctx.clearRect(0,0,this.canvas.width,this.canvas.height);
		this.player.draw(this.ctx);
		this.writeFPS(this.ctx , deltaTime);
	}

	writeFPS(ctx , deltaTime){
        this.fps = 1000 / deltaTime;
		ctx.font = "30px Arial";
		ctx.fillStyle = "red";
		ctx.fillText(`FPS : ${this.fps.toFixed(0)}` , 10, 40)
	}
}

